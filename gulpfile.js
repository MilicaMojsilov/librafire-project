var gulp = require('gulp');
var sass = require('gulp-sass');
var sassLint = require('gulp-sass-lint');



//scss to css task
gulp.task('scss', gulp.series(function() { 
    return gulp.src('src/scss/main.scss')
        .pipe(sass())
        .pipe(gulp.dest('dist/css'));
}));

//scss lint
gulp.task('scss-lint', gulp.series(function() { 
    return gulp.src('src/scss/*.scss')
		.pipe(sassLint({
        configFile: '.sass-lint.yml'
		}))
		.pipe(sassLint.format())
		.pipe(sassLint.failOnError())
}));


//watch task
gulp.task('default', gulp.series('scss', 'scss-lint', function() { 
    gulp.watch('src/**/*.scss', gulp.series('scss', 'scss-lint'));
}));